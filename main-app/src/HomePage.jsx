import {shallowEqual, useSelector} from 'react-redux';
import {capitalizeFirstAlphabet} from 'components/capitalizeFirstAlphabet'
 
const HomePage = () =>{

    const login = useSelector((state) => state.login, shallowEqual);

    return(
        <div>
            Home Page Content {login.userName}
            {login.isLoggedIn && <h1>Hi {login.userName}!!! You are Logged in. </h1>}
            {login.isLoggedIn && <h1>Capitalize First letter of user Name:  {capitalizeFirstAlphabet(login.userName)} </h1>}
        </div>
    )
}
export default HomePage;