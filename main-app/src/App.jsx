import React, {Suspense} from "react";
import ReactDOM from "react-dom";
import { Provider} from 'react-redux'

import "./index.scss";
import Footer from "components/Footer";
import homeStore from "./redux/store";

const HomePage  = React.lazy(() => import("./HomePage"));
const Header = React.lazy(() => import("components/Header"));

const App = () => (
<>
<Suspense  fallback={<div>Loading...</div>}>
<Header/>
</Suspense>
<Suspense  fallback={<div>Loading...</div>}>
<HomePage/>
</Suspense>
<Footer/>
</>
);
ReactDOM.render(<Provider store={homeStore}><App /> </Provider>, document.getElementById("app"));
