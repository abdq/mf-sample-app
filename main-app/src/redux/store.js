import {createStore, combineReducers} from 'redux';
import loginReducer from 'login/loginReducer';

const HOMETITLE = 'HOMETITLE';

export const addTitle = (data) => ({
  type: HOMETITLE,
  payload: data
});


const initialState = {
    homeTitle : '',
}

function homeReducer(state = initialState, action) {
  switch (action.type) {
    case HOMETITLE:
       state.homeTitle =  action.payload 
      return {...state}
    default:
     return {...state}
  }
}

const homeStore = createStore(
  combineReducers({
    home: homeReducer,
    login:loginReducer,
  })
);

export default homeStore;