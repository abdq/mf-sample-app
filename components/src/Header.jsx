import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';
import 'bootstrap/dist/css/bootstrap.css';
import Login from 'login/Login'
import { shallowEqual, useSelector } from 'react-redux';

const Header = () =>{


    const login = useSelector((state) => state.login, shallowEqual)

        return (
    <Navbar collapseOnSelect expand="lg" className="bg-red-500">
      <Container>
        <Navbar.Brand href="#home">Micro FE App</Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="me-auto">
            <Nav.Link href="#features">Home</Nav.Link>
            <Nav.Link href="#pricing">About Us</Nav.Link>
            <NavDropdown title="Components" id="collapsible-nav-dropdown">
              <NavDropdown.Item href="#action/3.1">Header</NavDropdown.Item>
              <NavDropdown.Item href="#action/3.2">
                Footer
              </NavDropdown.Item>
              <NavDropdown.Item href="#action/3.3">Buton</NavDropdown.Item>
            </NavDropdown>
          </Nav>
          <Nav>
             {login.isLoggedIn  && (
                      <Navbar.Text className="ms-auto">
                        Welcome, {login.userName}!
                      </Navbar.Text>
                    )}
            <Nav.Link href="#deets">Cart</Nav.Link>
            <Nav.Link href="#deets"><Login/></Nav.Link>
            
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}
export default Header;