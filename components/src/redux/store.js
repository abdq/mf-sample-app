import { createStore, combineReducers } from "redux";
import loginReducer from "login/loginReducer";

const compStore  = createStore(
  combineReducers({
    login: loginReducer,
  })
);

export default compStore;
