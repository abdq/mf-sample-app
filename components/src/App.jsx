import React from "react";
import ReactDOM from "react-dom";
import Header from "./Header";
import { Provider} from 'react-redux'


import "./index.scss";
import Footer from "./Footer";
import compStore from "./redux/store";

const App = () => (
  // <div className="text-3xl mx-auto max-w-6xl">
  <>
    <Header/>
    <Footer/>
    </>
  // </div>
);
ReactDOM.render(<Provider store={compStore}> <App /></Provider>, document.getElementById("app"));

