export const capitalizeFirstAlphabet = (str) =>{
// Check if input is a string
  if(str === null || str === undefined) return;
  if (typeof str !== 'string' || str.length === 0) {
    return str; // Return input as is if not a string or empty
  }

  // Capitalize the first letter and concatenate with the rest of the string
  return str.charAt(0).toUpperCase() + str.slice(1);// Check if input is a string
}