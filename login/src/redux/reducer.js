export const ISLOGGEDIN = "ISLOGGEDIN";
export const USERNAME = "USERNAME";

const initialState = {
  isLoggedIn: false,
  userName: "",
};

export default function loginReducer(state = initialState, action) {
  switch (action.type) {
    case ISLOGGEDIN:
      state.isLoggedIn = action.payload;
      return { ...state };
    case USERNAME:
      state.userName = action.payload;
      return { ...state };
    default:
      return { ...state };
  }
}
