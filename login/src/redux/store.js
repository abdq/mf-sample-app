import {createStore, combineReducers} from 'redux';
import loginReducer, { ISLOGGEDIN } from './reducer.js';
import { USERNAME } from './reducer.js';



export const isLoggedIn = (data) => ({
  type: ISLOGGEDIN,
  payload: data
});

export const setUserName = (data) => ({
  type: USERNAME,
  payload: data
});



const store = createStore(
  combineReducers({
    login: loginReducer,
  })
);

export default store;