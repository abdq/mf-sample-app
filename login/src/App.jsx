import React from "react";
import ReactDOM from "react-dom";
import { Provider, shallowEqual, useSelector } from 'react-redux'
import store from "./redux/store";

import "./index.scss";
import Login from "./Login";


const App = () => {
      const loggedIn = useSelector((state) => state.isLoggedIn, shallowEqual);

    const userName = useSelector((state) => state.userName, shallowEqual);
    return(
      <div className="mt-10 text-3xl mx-auto max-w-6xl">
    <div>Name: {userName}</div>
    <div>Framework: {loggedIn}</div>
    <div>Language: JavaScript</div>
    <div>CSS: Tailwind</div>
    <Login />
  </div>
    )
  
  }
ReactDOM.render(<Provider store={store}><App /> </Provider>, document.getElementById("app"));
