import { useState } from "react";
//import { useLoggedIn , login } from "./cart";
import "./index.scss";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { isLoggedIn, setUserName } from "./redux/store";

export default function Login(){

    //const loggedIn = false //useLoggedIn();


    const dispatch = useDispatch();
    const [showLogin, setShowLogin] = useState(true);

    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");

    const onLogin = async() =>{
        dispatch(isLoggedIn(true));
        dispatch(setUserName('abdul'))
    }

    //if(loggedIn) return null;

    return(
        <>
        <span onClick={() => setShowLogin(!showLogin)}>
            Sign In
        </span>
        {showLogin && (
            <div className=" bg-gray-200 absolute p-5 border-4 border-gray-500" style={{width: "300", top:"2.5rem"}}>
                <input 
                type="text"
                placeholder="User Name"
                value={username}
                onChange={(e) => setUsername(e.target.value)}
                className="border text-sm borger-gray-400 p-2 rounded-md w-full mb-2"
                />
                 <input 
                type="text"
                placeholder="Password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
                className="border text-sm borger-gray-400 p-2 rounded-md w-full mb-2"
                />

                <button className="bg-red-900 text-white py-2 px-5 rounded" id = "loginBtn" onClick={onLogin}>Login</button>
                {/* <button className="bg-red-900 text-white py-2 px-5 rounded" id = "loginBtn" onClick={() => login(username,password)}>Login</button> */}
            </div>
        )}
        
         </>
    )


}